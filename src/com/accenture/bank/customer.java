package com.accenture.bank;

public class customer {
	private int custid;
	private String name;
	private int age;
	private long addharno;
	private char gender;
	private long accno;
	private String address;
	private long contact;
	public int getCustId() {
		return custid;
	}
	public void setCustId(int custid) {
		this.custid=custid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age=age;
	}
	public long getAddharno() {
		return addharno;
	}
	public void setAddharno(long addharno) {
		this.addharno=addharno;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender=gender;
	}
	public long getAccno(){
		return accno;
	}
	public void setAccno(long accno) {
		this.accno=accno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address=address;
	}
	public long getContact() {
		return contact;
	}
	public void setContact(long contact) {
		this.contact=contact;
	}
	public customer() {
		System.out.println("BANKING APPLICATION");
	}
	public customer(int custid,String name,int age,long addharno,char gender,
			long accno,String address,long contact) {
		super();
		this.custid=custid;
		this.name=name;
		this.age=age;
		this.addharno=addharno;
		this.gender=gender;
		this.accno=accno;
		this.address=address;
		this.contact=contact;
	}
	@Override
	public String toString() {
		return "Customer [custid="+ custid+",name="+name+",age="+age+",addharno="
	+addharno+",gender="+gender+",accno="+accno+",address="+address+",contact="+contact
	+"]";
	}
}
